# Fed Up

[![Build Status](https://buildmaster.lubar.me/api/ci-badges/image?key=badges&$ApplicationId=47)](https://buildmaster.lubar.me/api/ci-badges/link?page=build&key=badges&$ApplicationId=47)

See [the wiki](https://git.lubar.me/ben/fed-up/wiki) for documentation.
