package main

import (
	"context"
	"log"
	"net"
	"net/http"

	"git.lubar.me/ben/fed-up/db"
	"github.com/urfave/cli/v2"
)

func Serve(ctx *cli.Context) error {
	if globalConfig == nil {
		return cli.Exit("No configuration was specified. Use --config=path/to/config.ini to specify the location of the configuration file.", 1)
	}

	if err := db.Init(globalConfig.Database.ConnectionString); err != nil {
		return err
	}

	server := &http.Server{
		Addr: globalConfig.HTTP.ListenAddress,
		BaseContext: func(ln net.Listener) context.Context {
			log.Println("Listening for HTTP requests on", ln.Addr())
			return ctx.Context
		},
		ErrorLog: log.New(ctx.App.ErrWriter, "", log.LstdFlags),
	}

	return server.ListenAndServe()
}
