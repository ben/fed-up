module git.lubar.me/ben/fed-up

go 1.13

require (
	github.com/lib/pq v1.3.0
	github.com/smartystreets/goconvey v1.6.4 // indirect
	github.com/urfave/cli/v2 v2.2.0
	gopkg.in/ini.v1 v1.55.0
)
