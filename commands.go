package main

import (
	"github.com/urfave/cli/v2"
	"gopkg.in/ini.v1"
)

var TopLevelCommands = []*cli.Command{
	{
		Name:  "serve",
		Usage: "Starts the Fed Up server",

		Action: Serve,
	},
	{
		Name:  "update",
		Usage: "Ensures the database is using the current layout",

		Action: Update,
	},
	{
		Name:  "internal",
		Usage: "Low-level commands that probably aren't interesting",

		Hidden:      true,
		Subcommands: InternalCommands,
	},
}

var InternalCommands = []*cli.Command{
	{
		Name:  "export-blank-config",
		Usage: "Outputs a configuration INI file with blank settings",

		Action: func(ctx *cli.Context) error {
			f := ini.Empty()
			if err := ini.ReflectFrom(f, &Config{}); err != nil {
				return err
			}

			_, err := f.WriteTo(ctx.App.Writer)

			return err
		},
	},
}
