package db

import (
	"database/sql"

	"github.com/lib/pq"
)

var db *sql.DB

func Init(dataSourceName string) error {
	conn, err := pq.NewConnector(dataSourceName)
	if err != nil {
		return err
	}

	db = sql.OpenDB(conn)
	return nil
}
