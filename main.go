package main

import (
	"fmt"
	"os"

	"github.com/urfave/cli/v2"
	"gopkg.in/ini.v1"
)

var App = cli.App{
	Name:  "fed-up",
	Usage: "Lightweight federated micro-blogging platform",

	Before: func(ctx *cli.Context) error {
		if cfgPath := ctx.Path("config"); cfgPath != "" {
			globalConfig = &Config{}
			if err := ini.MapTo(globalConfig, cfgPath); err != nil {
				// error out here so the cli package doesn't
				// print usage information

				fmt.Fprintln(ctx.App.ErrWriter, "Could not parse configuration:", err)
				os.Exit(1)

				return err
			}
		}

		return nil
	},

	Flags: []cli.Flag{
		&cli.PathFlag{
			Name:      "config",
			Aliases:   []string{"c"},
			Usage:     "path to config.ini file to load",
			TakesFile: true,
		},
	},

	Commands: TopLevelCommands,

	EnableBashCompletion:   true,
	UseShortOptionHandling: true,

	Writer:    os.Stdout,
	ErrWriter: os.Stderr,
}

func main() {
	if err := App.Run(os.Args); err != nil {
		fmt.Fprintln(os.Stderr, "Fatal error:", err)
		os.Exit(1)
	}
}
