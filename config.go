//go:generate sh -c "go run . internal export-blank-config > config.example.ini"

package main

// Config holds the lowest level configuration information for Fed Up.
//
// Only information that is required to start up and cannot be changed at
// runtime is stored in this structure.
type Config struct {
	Plugins []string `ini:",,allowshadow" comment:"Plugins to load. Specify each plugin as a separate \"Plugin = \" line."`

	HTTP struct {
		ListenAddress string `comment:"Address to listen for connections on.\nExample: :8080 for all available IP addresses on port 8080."`
	}

	Database struct {
		ConnectionString string `comment:"For a description of the format of connection strings, see:\nhttps://www.postgresql.org/docs/current/static/libpq-connect.html#LIBPQ-CONNSTRING"`
	}

	Files struct {
		Provider string `comment:"The file storage provider. Defaults to \"local\" if empty.\nOnly local is available by default. Plugins can add additional providers."`
		BasePath string `comment:"The base path for stored files. The local provider interprets this\nas a filesystem path. Other providers may handle it differently.\nIf this is blank, the local provider does not allow storing files."`
	}
}

var globalConfig *Config
