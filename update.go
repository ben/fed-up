package main

import (
	"git.lubar.me/ben/fed-up/db"
	"github.com/urfave/cli/v2"
)

func Update(ctx *cli.Context) error {
	if globalConfig == nil {
		return cli.Exit("No configuration was specified. Use --config=path/to/config.ini to specify the location of the configuration file.", 1)
	}

	if err := db.Init(globalConfig.Database.ConnectionString); err != nil {
		return err
	}

	panic("TODO")
}
